import java.rmi.Remote;
import java.rmi.RemoteException;

public interface CurrencyConverter extends Remote{
	double convert(String from, String to, int amount) throws RemoteException;
}
