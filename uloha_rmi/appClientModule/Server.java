import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.server.UnicastRemoteObject;

public class Server extends UnicastRemoteObject implements CurrencyConverter{
	public static void main(String[] args){
		try {
 
            LocateRegistry.createRegistry(1099);
 
            Server server = new Server();
            Naming.rebind("//0.0.0.0/Converter", server);
 
            System.out.println("Server started...");
 
        } catch (Exception e) {
            System.out.println("Error: " + e.getLocalizedMessage());
        }
	}

	/* (non-Java-doc)
	 * @see java.lang.Object#Object()
	 */
	public Server() throws RemoteException {
		super();
	}

	@Override
	public double convert(String from, String to, int amount) {
		if(from.equals("EUR")){
			if(to.equals("USD")){
				return amount * 1.16195;
			}
			
			if(to.equals("GBP")){
				return amount * 0.8884089;
			}
		}
		
		if(from.equals("USD")){
			if(to.equals("EUR")){
				return amount * 0.86062223;
			}
			
			if(to.equals("GBP")){
				return amount * 0.764584448;
			}
		}
		
		if(from.equals("GBP")){
			if(to.equals("EUR")){
				return amount * 1.12560781;
			}
			
			if(to.equals("USD")){
				return amount * 1.3079;
			}
		}
		
		return 0;
	}

}