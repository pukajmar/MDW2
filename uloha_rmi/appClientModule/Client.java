import java.rmi.Naming;

public class Client {
	public static void main(String[] args) throws Exception{
        CurrencyConverter client = (CurrencyConverter)Naming.lookup("//localhost/Converter");
        System.out.println(client.convert("GBP", "EUR", 1));
        System.out.println(client.convert("EUR", "USD", 100));
        System.out.println(client.convert("GBP", "USD", 50));
    }
}
