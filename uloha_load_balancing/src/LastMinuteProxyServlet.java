

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Random;
import java.net.HttpURLConnection;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



/**
 * Servlet implementation class LastMinuteProxyServlet
 */
public class LastMinuteProxyServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LastMinuteProxyServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		List<String> availableServices = new ArrayList();
		List<String> healthyServices = new ArrayList();
		
		availableServices.add("http://147.32.233.18:8888/MI-MDW-LastMinute1/list");
		availableServices.add("http://147.32.233.18:8888/MI-MDW-LastMinute2/list");
		availableServices.add("http://147.32.233.18:8888/MI-MDW-LastMinute3/list");
		
		for(String serviceUrl : availableServices){
			HttpURLConnection connection = (HttpURLConnection) (new URL(serviceUrl)).openConnection();
			connection.setRequestMethod("GET");
			int code = connection.getResponseCode();
			
			if(code == 200){
				healthyServices.add(serviceUrl);
			}
		}
		
		Random randomizer = new Random();
		String url = healthyServices.get(randomizer.nextInt(healthyServices.size()));
		
        HttpURLConnection connection = (HttpURLConnection) (new URL(url)).openConnection();
        // HTTP method
        connection.setRequestMethod("GET");
        
        Enumeration<String> headerNames = request.getHeaderNames();
		while (headerNames.hasMoreElements()) {
			String headerName = headerNames.nextElement();
			
			connection.setRequestProperty(headerName, request.getHeader(headerName));
		}
		
        // copy body
        BufferedReader inputStream = new BufferedReader(
                new InputStreamReader(connection.getInputStream()));
        String inputLine;
        ServletOutputStream sout = response.getOutputStream();
        while ((inputLine = inputStream.readLine()) != null) {
            sout.write(inputLine.getBytes());
        }
        // close
        inputStream.close();
        sout.flush();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
