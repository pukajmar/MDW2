

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

/**
 * Servlet implementation class records
 */
public class records extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public records() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		PrintWriter out = response.getWriter();
		try {
			Properties prop = new Properties();
			
			prop.put(Context.PROVIDER_URL, "t3://localhost:7001");
			prop.put(Context.INITIAL_CONTEXT_FACTORY, "weblogic.jndi.WLInitialContextFactory");
		
		
			InitialContext ctx = new InitialContext(prop);
			
			DataSource datasource = (DataSource) ctx.lookup("mimdw");
		
			Connection con;
			try {
				con = datasource.getConnection();
	
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT * FROM records");
			
			
			
			out.println("<html>");
	        out.println("<body>");
	        out.println("<table>");
	        out.println("<tr><th>ID</th><th>Type</th><th>Location</th><th>Capacity</th><th>Occupied</th><th>Trip</th><th>Person</th></tr>");
			while (rs.next()) {
		        int id = rs.getInt("id");
		        String type = rs.getString("type");
		        String location = rs.getString("location");
		        int capacity = rs.getInt("capacity");
		        int occupied = rs.getInt("occupied");
		        int trip = rs.getInt("trip");
		        String person = rs.getString("person");
		        
		        
		        
		        out.println("<tr>");
		        out.println("<td>" + id + "</td>");
		        out.println("<td>" + type + "</td>");
		        out.println("<td>" + location + "</td>");
		        out.println("<td>" + capacity + "</td>");
		        out.println("<td>" + occupied + "</td>");
		        out.println("<td>" + trip + "</td>");
		        out.println("<td>" + person + "</td>");
		        out.println("</tr>");
		        
		    }
			
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		} catch (NamingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		out.println("</table>");
		out.println("</body>");
        out.println("</html>");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
