package uloha_jms_order_processor_maven;

import java.util.Hashtable;

import javax.jms.DeliveryMode;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueReceiver;
import javax.jms.QueueSender;
import javax.jms.QueueSession;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

public class Main implements MessageListener{
	
	// connection factory
    private QueueConnectionFactory qconFactory;
 
    // connection to a queue
    private QueueConnection qcon;
    
    private QueueConnection sendQcon;
 
    // session within a connection
    private QueueSession qsession;
    
    private QueueSession sendQsession;
 
    // queue receiver that receives a message to the queue
    private QueueReceiver qreceiver;
 
    // queue where the message will be sent to
    private Queue queue;
    
    private TextMessage msg;
    
    private QueueSender qsender;
 
    // callback when the message exist in the queue
    public void onMessage(Message msg) {
        try {
            String msgText;
            if (msg instanceof TextMessage) {
                msgText = ((TextMessage) msg).getText();
            } else {
                msgText = msg.toString();
            }
            System.out.println("Message Received: " + msgText);
                        
            
            if(msgText.toLowerCase().contains("booking".toLowerCase())){
            	send("jms/mdw-booking-queue", msgText);
            }else if(msgText.toLowerCase().contains("new trip".toLowerCase())){
            	send("jms/mdw-new-trip-queue", msgText);
            }
            
        } catch (JMSException jmse) {
            System.err.println("An exception occurred: " + jmse.getMessage());
        } catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
 
    // create a connection to the WLS using a JNDI context
    public void init(Context ctx, String queueName)
            throws NamingException, JMSException {
 
        qconFactory = (QueueConnectionFactory) ctx.lookup("jms/mdw-cf");
        qcon = qconFactory.createQueueConnection();
        qsession = qcon.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
        queue = (Queue) ctx.lookup(queueName);
 
        qreceiver = qsession.createReceiver(queue);
        qreceiver.setMessageListener(this);
 
        qcon.start();
    }
 
    // close sender, connection and the session
    public void close() throws JMSException {
        qreceiver.close();
        qsession.close();
        qcon.close();
    }
    
    public void closeSend() throws JMSException {
        qsender.close();
        sendQsession.close();
        sendQcon.close();
    }
    
    
    public void initSend(Context ctx, String queueName)
    		throws NamingException, JMSException {
    	 
    		
	    	sendQcon = qconFactory.createQueueConnection();
	        sendQsession = sendQcon.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
     
            // lookups the queue using the JNDI context
    		Queue sendQueue = (Queue) ctx.lookup(queueName);
     
            // create sender and message
            qsender = sendQsession.createSender(sendQueue);
            msg = sendQsession.createTextMessage();
        }
    
    public void send(String queueName, String message) throws Exception {
   	 
        // create a JNDI context to lookup JNDI objects (connection factory and queue)
        Hashtable<String, String> env = new Hashtable<String, String>();
        env.put(Context.INITIAL_CONTEXT_FACTORY, "weblogic.jndi.WLInitialContextFactory");
        env.put(Context.PROVIDER_URL, "t3://localhost:7001");

        InitialContext ic = new InitialContext(env);
        initSend(ic, queueName);

        // send the message and close
        try {
            msg.setText(message);
            qsender.send(msg, DeliveryMode.PERSISTENT, 8, 0);
            System.out.println("The message was sent to the destination " +
                    qsender.getDestination().toString());
        } finally {
        	closeSend();
        }
    }
 
    // start receiving messages from the queue
    public void process(String queueName) throws Exception {
    	Hashtable<String, String> env = new Hashtable<String, String>();
        env.put(Context.INITIAL_CONTEXT_FACTORY, "weblogic.jndi.WLInitialContextFactory");
        env.put(Context.PROVIDER_URL, "t3://localhost:7001");

        InitialContext ic = new InitialContext(env);
        init(ic, queueName);
 
        System.out.println("Connected to " + queue.toString() + ", receiving messages...");
        
        
        try {
            synchronized (this) {
                while (true) {
                    this.wait();
                }
            }
        } finally {
            close();
            System.out.println("Finished.");
        }
    }
    
    public static void main(String[] args) throws Exception {
        // input arguments
        String queueName = "jms/mdw-order-queue" ;
 
        // create the producer object and receive the message
        Main consumer = new Main();
        consumer.process(queueName);
    }

}
