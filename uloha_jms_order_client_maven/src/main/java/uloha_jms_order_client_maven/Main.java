package uloha_jms_order_client_maven;

import java.util.List;
import java.util.ArrayList;
import java.util.Hashtable;

import javax.jms.DeliveryMode;
import javax.jms.JMSException;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueSender;
import javax.jms.QueueSession;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

public class Main {
	
	private QueueConnectionFactory qconFactory;
	 
    // connection to a queue
    private QueueConnection qcon;

    // session within a connection
    private QueueSession qsession;

    // queue sender that sends a message to the queue
    private QueueSender qsender;

    // queue where the message will be sent to
    private Queue queue;

    // a message that will be sent to the queue
    private TextMessage msg;
    
    public void init(Context ctx, String queueName)
		throws NamingException, JMSException {
	 
        // create connection factory based on JNDI and a connection
    	qconFactory = (QueueConnectionFactory) ctx.lookup("jms/mdw-cf");
        qcon = qconFactory.createQueueConnection();
 
        // create a session within a connection
        qsession = qcon.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
 
        // lookups the queue using the JNDI context
        queue = (Queue) ctx.lookup(queueName);
 
        // create sender and message
        qsender = qsession.createSender(queue);
        msg = qsession.createTextMessage();
    }
    
    public void close() throws JMSException {
        qsender.close();
        qsession.close();
        qcon.close();
    }
    
    public void send(String queueName, String message) throws Exception {
    	 
        // create a JNDI context to lookup JNDI objects (connection factory and queue)
        Hashtable<String, String> env = new Hashtable<String, String>();
        env.put(Context.INITIAL_CONTEXT_FACTORY, "weblogic.jndi.WLInitialContextFactory");
        env.put(Context.PROVIDER_URL, "t3://localhost:7001");

        InitialContext ic = new InitialContext(env);
        init(ic, queueName);

        // send the message and close
        try {
            msg.setText(message);
            qsender.send(msg, DeliveryMode.PERSISTENT, 8, 0);
            System.out.println("The message was sent to the destination " +
                    qsender.getDestination().toString());
        } finally {
            close();
        }
    }
	
	public static void main(String[] args) throws Exception{
		
		System.out.println("test");
		
		List<String> messages = new ArrayList();
		
		messages.add("New trip 1");
		messages.add("Booking 1");
		messages.add("Booking 2");
		messages.add("Booking 3");
		messages.add("New trip 2");
		messages.add("Booking 4");
		messages.add("New trip 3");
		messages.add("Booking 5");
		messages.add("New trip 4");
		messages.add("New trip 5");

        String queueName = "jms/mdw-order-queue" ;

        // create the producer object and send the message
        Main producer = new Main();
        
        for(String msg : messages){
        	producer.send(queueName, msg);
        }
	}

	/* (non-Java-doc)
	 * @see java.lang.Object#Object()
	 */
	public Main() {
		super();
	}

}