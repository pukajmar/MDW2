package RMI;

import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.server.UnicastRemoteObject;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import example.Trip;

public class TripServer extends UnicastRemoteObject implements TripInterface{
    private static final long serialVersionUID = 1L;
 
    protected TripServer() throws RemoteException {
        super();
    }
    
	@Override
	public List<Trip> getAllTrips() throws Exception {
	    String driver = "org.apache.derby.jdbc.EmbeddedDriver";
	    Class.forName(driver).newInstance();
	    String protocol = "jdbc:derby:";
	    Connection conn = DriverManager.getConnection(protocol + "derbyDB;create=true");
	            
	    Statement	 stmt = conn.createStatement();
	    ResultSet rs = stmt.executeQuery("SELECT id, name, capacity FROM Trip");
	    List<Trip> list = new ArrayList<>();
	    while (rs.next()) {
	    	list.add(new Trip(Integer.parseInt(rs.getString("id")),rs.getString("name"),Integer.parseInt(rs.getString("capacity"))));
	    }
	    return list;
	}

	@Override
	public void addTrip(Trip trip) throws Exception {
		String driver = "org.apache.derby.jdbc.EmbeddedDriver";
		Class.forName(driver).newInstance();
		String protocol = "jdbc:derby:";
	   	Connection conn = DriverManager.getConnection(protocol + "derbyDB;create=true");
	   	Statement stmt = conn.createStatement();
	    stmt.executeUpdate("Insert into Trip (id, name,capacity) Values ("+trip.getId()+"'"+trip.getName()+"',"+trip.getCapacity()+")");
	}
	
	public static void main(String[] args) throws Exception{
		TripServer server = new TripServer();
		LocateRegistry.createRegistry(1099);
		Naming.rebind("//0.0.0.0/TripServer", server);
	}

	@Override
	public void addTrip(String name, String capacity) throws Exception{
		String driver = "org.apache.derby.jdbc.EmbeddedDriver";
		Class.forName(driver).newInstance();
		String protocol = "jdbc:derby:";
	   	Connection conn = DriverManager.getConnection(protocol + "derbyDB;create=true");
	   	Statement stmt = conn.createStatement();
	    stmt.executeUpdate("Insert into Trip (name,capacity) Values ('"+name+"',"+capacity+")");
		
	}
	
	
}
