package RMI;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

import example.Trip;
public interface TripInterface extends Remote{
    public List<Trip> getAllTrips() throws RemoteException, Exception;
    public void addTrip(Trip trip)  throws RemoteException, Exception;
	public void addTrip(String parameter, String parameter2) throws Exception;
}
