package mdw;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class booking
 */
public class booking extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public booking() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	      HttpSession session = request.getSession(true);
	         
	      
	      String newState = "";
	      
	      if (session.isNew()) {
	    	 newState = "new";
	         session.setAttribute("state", "new");
	      }else{
	    	  String state = (String)session.getAttribute("state");
	    	  
	    	  if(state.equals("new")){
	    		  newState = "payment";
	    		  session.setAttribute("state", newState);
	    	  }
	    	  
	    	  if(state.equals("payment")){
	    		  newState = "completed";
	    		  session.invalidate();
	    	  }
	      }
		
		response.getWriter().append("Your booking state: ").append(newState).append("\n");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
