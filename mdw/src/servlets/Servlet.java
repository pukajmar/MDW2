package servlets;

import java.io.IOException;
import java.rmi.Naming;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import RMI.TripInterface;
import example.DB;

/**
 * Servlet implementation class Servlet
 */
public class Servlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Servlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	    String driver = "org.apache.derby.jdbc.EmbeddedDriver";
	    try {
	    	TripInterface client = (TripInterface) Naming.lookup("//127.0.0.1/TripServer");
	    	client.addTrip(request.getParameter("trip"),request.getParameter("capacity") );
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	    //db.addTrip(request.getParameter("trip"), Integer.parseInt(request.getParameter("capacity")));
		System.out.println(request.getParameter("trip")+Integer.parseInt(request.getParameter("capacity")));
		response.getWriter().append("Přidáno!");
		response.sendRedirect("/MIMDW_2/Index.jsp");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
