package servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import example.DB;

/**
 * Servlet implementation class ServletTrip
 */
public class ServletTrip extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletTrip() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		DB db = DB.getInstance();
		db.addBooking(request.getParameter("name"), Integer.parseInt(request.getParameter("tripId")));
		//(request.getParameter("trip"), Integer.parseInt(request.getParameter("capacity")));
		//System.out.println(request.getParameter("trip")+Integer.parseInt(request.getParameter("capacity")));
		response.getWriter().append("Přidáno!");
		response.sendRedirect("/MIMDW_2/Index.jsp");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
