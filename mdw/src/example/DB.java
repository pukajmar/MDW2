package example;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.List;


public class DB {
    private static DB instance = null;
    private Hashtable<Integer, Trip> ht = new Hashtable();
    private List<Booking> bookings = new ArrayList<>();
 
    public static DB getInstance() {
        if (instance == null)
            instance = new DB();
        instance.addObject(new Trip(1, "Prague",2));
        instance.addObject(new Trip(2, "Brno",3));
        instance.addObject(new Trip(0, "London",2));
        return instance;
    }
    public void addObject(Trip t){
        ht.put(t.getId(), t);
    }
    
    public void addTrip(String name, int capacity){
        ht.put(ht.size(),new Trip(ht.size(),name,capacity));
        System.out.println(ht.size());
    }
    
    public void addBooking(String name, int tripId){
    	bookings.add(new Booking(name, ht.get(tripId)));
    }
    
    
    public Trip getObject(int id) {
        return ht.get(id);
    }
    public int getLength(){
    	return ht.size();
    }
}