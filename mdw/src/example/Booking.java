package example;

public class Booking {
	String name;
	Trip trip;
	
	
	
	public Booking(String name, Trip trip) {
		super();
		this.name = name;
		this.trip = trip;
		trip.decreaseCapacity();
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Trip getTrip() {
		return trip;
	}
	public void setTrip(Trip trip) {
		this.trip = trip;
	}
	
}
