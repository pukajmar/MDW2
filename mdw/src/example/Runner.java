package example;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Runner {
    public static void main(String[] args) throws Exception {
    	//DB db = DB.getInstance();
        //db.addObject(new Trip(1, "Prague"));
        //System.out.println(db.getObject(1).getName());
    	
        String driver = "org.apache.derby.jdbc.EmbeddedDriver";
        Class.forName(driver).newInstance();
        String protocol = "jdbc:derby:";
        Connection conn = DriverManager.getConnection(protocol + "derbyDB;create=true");
        init(conn);

    }
    
    public static void init(Connection conn) throws SQLException {
        DatabaseMetaData dbmd = conn.getMetaData();
        ResultSet rs = dbmd.getTables(null, null, "Trip", null);
        if (rs.next()) {
            System.out.println("Table " +  rs.getString(3) + " exists");
        } else {
            Statement stmt = conn.createStatement();
            String query = "CREATE TABLE Trip (ID INTEGER PRIMARY KEY NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1), NAME VARCHAR(30), CAPACITY INT )";
            stmt.executeUpdate(query);
            stmt.close();
 
            stmt = conn.createStatement();
            query = "INSERT INTO Trip (name, capacity) VALUES ('Prague', 2),('London', 10),('Berlin', 30)";
            stmt.executeUpdate(query);
            stmt.close(); 
        }
    }
    
    
}